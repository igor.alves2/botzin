O botzin foi criado com o objetivo de facilitar os downloads dos cursos da Origamid.

Recomendações:
1. Não alterar as dimensões do navegador.

Bugs ainda não corrigidos:
1. Alguns vídeos baixados são corrompidos e não é possivel reproduzí-los.
2. Ao descomentar a linha que fecha o navegador automaticamente, o bot dá erro.
