const readlineSync = require('readline-sync');
const puppeteer = require('puppeteer');
const https = require('https');
const fs = require('fs');

// Lista de cursos existentes na Origamid
const courses = [
  { name: 'UX Heuristicas', url: 'https://www.origamid.com/curso/ux-design-heuristicas/' },
  { name: 'React', url: 'https://www.origamid.com/curso/react-completo' },
  { name: 'Web Design', url: 'https://www.origamid.com/curso/web-design-completo/' },
  { name: 'Flexbox', url: 'https://www.origamid.com/curso/css-flexbox/' },
  { name: 'Grid', url: 'https://www.origamid.com/curso/css-grid-layout/' },
  { name: 'UI Avançado', url: 'https://www.origamid.com/curso/ui-design-avancado/' },
  { name: 'JS ES6', url: 'https://www.origamid.com/curso/javascript-completo-es6/' },
  { name: 'WP', url: 'https://www.origamid.com/curso/wordpress-como-cms/' },
  { name: 'CSS Avançado Posicionamento', url: 'https://www.origamid.com/curso/css-avancado-posicionamento/'},
  { name: 'WP Rest API Dogs', url: 'https://www.origamid.com/curso/wordpress-rest-api-dogs/' },
  { name: 'Redux', url: 'https://www.origamid.com/curso/redux-com-react/' },
  { name: 'Tipografia Avançada', url: 'https://www.origamid.com/curso/tipografia-avancada/' },
  { name: 'SASS', url: 'https://www.origamid.com/curso/css-com-sass/' },
  { name: 'Adobe XD', url: 'https://www.origamid.com/curso/adobe-xd/' },
  { name: 'Vue 2', url: 'https://www.origamid.com/curso/vue-js-completo/' },
  { name: 'Woocommerce', url: 'https://www.origamid.com/curso/woocommerce-como-cms/' },
  { name: 'Bootstrap', url: 'https://www.origamid.com/curso/bootstrap-4/' },
  { name: 'Automação Frontend NPM', url: 'https://www.origamid.com/curso/automacao-front-end-com-npm/' },
  { name: 'JS e Jquery', url: 'https://www.origamid.com/curso/javascript-e-jquery/' },
  { name: 'WP Rest API', url:'https://www.origamid.com/curso/wordpress-rest-api/'},
];

async function botzin () {
  console.log('Abrindo o browser...');
  const browser = await puppeteer.launch({ 
    // Caso queira que o navegador seja aberto, coloque o headless como false
    headless:  true,
    // Só necessário no Debian
    // args: ['--single-process', '--no-zygote', '--no-sandbox',] 
  });

  const page = await browser.newPage();

  // Aumenta o tamanho de exibição do navegador 
  await page.setViewport({ width: 1000, height: 1300});

  // Acessa a página de login
  console.log('Fazendo login...');  
  await page.goto('https://www.origamid.com/minha-conta/');

  // Digita o email e a senha nos inputs
  await page.waitForSelector('[name="username"]');
  await page.type('[name="username"]', 'origamid@injunior.com.br');
  await page.type('[type="password"]', 'Origamid@2019');

  // Aceita os cookies
  await page.waitForSelector('.lgdp-save');
  await page.click('.lgdp-save');

  // Clica no botão de login 
  await page.waitForSelector('[type="submit"]');
  await page.click('[value="Acessar"]');

  // Pergunta qual curso o usuário quer baixar 
  console.log('Escolha o curso...'); 
  const options = courses.map( course => course.name);
  const indexCourse = readlineSync.keyInSelect(options, 'Qual curso deseja baixar?');
  const courseSelected = courses[indexCourse];

  // Acessa a página do curso escolhido
  await page.goto(courseSelected.url);

  await page.waitForSelector('li.aula a');
  const classLinks = await page.$$eval('li.aula a', links => links.map( link => link.attributes.href.nodeValue ))
  
  console.log('Criando pasta do curso, caso não exista...');
  const folderName = `./${courseSelected.name}/`
  if(!fs.existsSync(folderName)) {
    fs.mkdirSync(folderName)
  };

  console.log('Baixando aulas...');

  ( async() => {
    for(let [index, link] of classLinks.entries()){
      await page.goto(`https://www.origamid.com${link}`)
      await page.waitForSelector('.assets-download');
      const downloadPage = await page.$eval('.assets-download > a', downloadBtn  => downloadBtn.attributes.href.nodeValue);
      await page.goto(downloadPage);
      
      const request = await https.get(downloadPage, response => {
        const fileName = `aula-${index + 1}.mp4`
        const fileStream = fs.WriteStream(`${ folderName + fileName}`);
        response.pipe(fileStream);
        fileStream.on('finish', () => {
          fileStream.close()
        });
        fileStream.on('error',(err) => {console.log(err)});
    
      });
      
      request.end();
    };

    console.log('Download concluído. Aproveite o curso!');
  })();

  // await browser.close();
};

botzin();

